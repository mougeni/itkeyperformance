#' Get ping data from google drive or where it is stored.
#'
#'This is a function for getting from google drive the ping data for several days.
#'You have to specify the number of days and the last date.
#'The function will automatically create a new folder called pingfolder.
#'P
#'
#'
#'
#'
#'
#'
#'
#'ing data downloaded will be kept there.
#'
#'
#'
#' @param nb Number of days you need to see the evolution of the connection.
#' @param djour The day corresponding to the last day of nb variable.
#' @param fromdrive Logical variable indicatiting whetheir data should be downloaded from drive. The default is TRUE.
#' @param l.path The path where the pingdata is stored after downloading.
#'
#' @return A vector containg the ping data.
#' @export get_data
#'
#' @examples
#' \dontrun{
#'
#' pingdata <- get_data(djour = "2018-12-28")
#'
#' }
#'
get_data <- function(nb = 3, djour = Sys.Date() - 1, fromdrive = T, l.path = "pingfolder",
                     name_ntwrk = c("4G", "4G-1", "4G-2", "4G-3", "ADSL", "VSAT")){ #name_ntwrk = c("4G", "VSAT", "4G-2", "ADSL")

  #Faire en sorte que pour tout le monde, la version lue soit en englais
  #pour éviter parfois des dates écrites en allemand pour les uns et en
  #espagnole pour les autres quand ils utilisent le package,
  #alors que c'est écrit en francais pour tous.

  Sys.setlocale("LC_TIME", "English")


  #Definir le nombre de jour limite en terme de jour

  limitday <- as.Date(djour) - nb + 1 # the start date calculated here

  nb <- as.numeric(nb)


  #Option pour prendre les données en ligne ou localement
  # si elles  sont stockées quelque part

  if(fromdrive == T){

    all <- drive_ls(path = "rclone", pattern =  ".zip")

    all <-
      all[, c(1, 2)]

  } else {

    all <- list.files(path = l.path, pattern = "*.zip", full.names = TRUE) %>%
      plyr::ldply(.data = ., .fun = unzip, exdir = l.path) %>%
      mutate(name = substr(V1, nchar(V1)-18, nchar(V1))) #comme le chemin change il faut juste prendre les derniers qui nous interessent
  }



  #Pour separer le nom des network et les dates puisqu'ils sont collés

  all <- all %>%
    separate(name, c("namenetwork", "ddate"), sep = "_") %>%
    mutate(ddate = as.Date(str_sub(ddate,1, 10)))


  #Vérifier si les données sont disponibles pour la requette

  dateexist <- limitday > max(all$ddate)

  if(dateexist) stop("No data found for your request!")



  all <-
    all %>%
    filter(ddate >= limitday, ddate <= djour)

  p <- c()

  for(i in seq_len(nrow(all))){

    if(fromdrive == T){
      temp <- tempfile(fileext = ".zip")

      drive_download(
        as_id(all$id[i]), path = temp, overwrite = TRUE, verbose = F)

      # to unzip

      out <- unzip(temp, exdir = tempdir())
    } else  out <- all$V1[i]

    # to take the name of the networks

    namenet <- all$namenetwork[i]


    #I read the ping and I paste the name of each network  to
    #identify the packets received to their respective network

    ping <- readLines(out)

    nmnet <- gl(1,length(ping), labels = namenet)
    ping <- paste0(ping, "_", nmnet)
    p <- c(p, ping)

  }

  #To delete NA if there is and I create a folder that should
  #contain the ping data

  p <- na.omit(p)
  dir.create("pingfolder")
  write.csv(p, file = "pingfolder/pingdata.csv")


  #To get a summary of the ping

  dt <- dtb_ping("pingfolder/pingdata.csv", network_name  = name_ntwrk)
  dtb <- as.data.frame(dt[[1]])

  summary(dtb[, c(1:5)])

}

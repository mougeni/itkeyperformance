#' Import and export the ticket data and provide the report.
#'
#' @param path The path...
#' @param startd  the start...
#' @param endd  The end...
#' @param duration  The duration...
#' @param report Yield the report
#' @param type
#'
#' @return the report or a message telling what's happened after the run.
#' @export
#'
#' @examples
#' \dontrun{
#'helpdesk(path = "ticket_export.csv", startd, endd, duration, report = TRUE, type = c("detailed", "summary"), table = c("A", "B", "C"))
#' }
#'
helpdesk <- function(path = "ticket_export.csv", path_report = "ITtest", startd = NA,
                     endd = Sys.Date() - 1, duration = 7, report = TRUE,
                      type = "short",  table = c("A", "B", "C")){


  Sys.setlocale("LC_TIME", "English")


  yesterday <- Sys.Date() - 1
  startd <- as.Date(startd)
  endd <- as.Date(endd)

  if(length(type) == 2) stop("Please choose either short or long for the type, not both")



  #Error messages
  ###############

  if(is.na(startd) & is.na(duration) & is.na(endd)) {

    stop("You did not specified arguments in the function, please fill at least two or use the default values  by typing helpdesk().")

  }



  if(!is.na(startd) & is.na(duration) & is.na(endd)) {

    stop("When you defined the start date, you should define also the end date or the duration, please fill the arguments endd or duration .")

  }

  if(is.na(startd) & is.na(duration) & !is.na(endd)) {

    stop("You did not specified end date or duration , please fill the arguments endd or duration .")

  }

  if(is.na(startd) & !is.na(duration) & is.na(endd)) {

    stop("You did not specified the start date and the end date, only the duration have been specified , please fill the arguments endd or startd.")

  }




if(duration < 0){
    stop("duration should not be a negative number")
  }


  if(is.na(startd) & is.na(endd) & is.na(duration)){

    stop("Please you should specify at least two arguments")

 }


#---------------------------------------


if(is.na(startd) & !is.na(endd) & !is.na(duration)){



  if(type == "short"){



      if(duration == 0) {

        nb <- as.Date(endd)

      } else {

        nb <- as.Date(endd) - duration + 1
      }

      if( endd == Sys.Date() - 1) {




      }


    import_tick_data(path, startdate = nb,  enddate = endd, duration.unit = "days", duration = duration)

    dat <- manipulate_tick_data("tickfolder/tickdata_week.csv")
    k <- as.data.frame(dat[[1]]) %>%
      separate(created.on, c("date", "jour"), sep = "_")

    endd <- max(unique(as.Date(k$date)))
    startdate <- min(unique(as.Date(k$date)))

    cat("Start date : ", startdate, "\n duration : ", as.character(duration), "\n end date :", as.character(endd), "\n")

    if(report == T){

    render_report(path = path_report, type.report = "helpdesk", table)




      }

  }

    if(type == "long"){


      # duration = duration * 28


      if(duration == 0) {

        nb <- as.Date(endd)

      } else   nb <- as.Date(endd) - duration + 1



      # if( endd == Sys.Date() - 1) {
      #
      #
      #   cat("Start date : ", as.character(nb), "\n Duration : ", duration, "\n End date :", as.character(endd), "\n")
      #
      # }

    import_tick_data(path = path, startdate = nb, enddate = endd, duration.unit = "months", duration = duration)


    dat <- manipulate_tick_data("tickfolder/tickdata_month.csv")
    k <- as.data.frame(dat[[1]]) %>%
      separate(created.on, c("date", "jour"), sep = "_")

    endd <- max(unique(as.Date(k$date)))
    startdate <- min(unique(as.Date(k$date)))

    cat("Start date : ", startdate, "\n duration : ", as.character(duration), "\n end date :", as.character(endd), "\n")



      if(report == T) render_report(path = path_report, type.report = "helpdesk summary", table)




    }


  }



 #............

  if(!is.na(startd) & is.na(endd)){


    endd <- as.Date(startd) + duration - 1




    if(type == "short"){

      import_tick_data(path, startdate = startd, duration = duration, enddate = endd, duration.unit = "days")


      dat <- create_data_week("tickfolder/tickdata_week.csv")
      k <- as.data.frame(dat[[2]]) %>%
        separate(Date_created, c("date", "jour"), sep = " - ")
      endd <- max(unique(as.Date(k$date)), na.rm = T)
      startdate <- min(unique(as.Date(k$date)), na.rm = T)


      cat("Start date : ", startdate, "\n duration : ", as.character(duration), "\n end date :", as.character(endd), "\n")


      if(report == T)  render_report(path = path_report, type.report = "helpdesk", table)





    }




    if(type == "long"){

     # duration <- duration * 28

      if(duration == 0) {

        nb <- as.Date(startd) + duration

      } else {

        nb <- as.Date(startd) + duration - 1
      }

      import_tick_data(path = path, startdate = startd,  enddate = nb, duration.unit = "months", duration = duration)

      dat <- manipulate_tick_data("tickfolder/tickdata_month.csv")
      k <- as.data.frame(dat[[1]]) %>%
        separate(created.on, c("date", "jour"), sep = "_")

      endd <- max(unique(as.Date(k$date)))
      startdate <- min(unique(as.Date(k$date)))

      cat("Start date : ", startdate, "\n duration : ", duration, "\n end date :", as.character(endd), "\n")




      if(report == T) render_report(path = path_report, type.report = "helpdesk summary", table)






    }

  }

#..............................


 if(!is.na(startd) & !is.na(endd) & !is.na(duration)){




   if(as.Date(startd) > as.Date(endd)){

     stop("startd should not be greater than endd, please check your entries")

   }






    d.change <- identical(duration, 7)
    endd.change <- identical(endd, yesterday)

    if(d.change == F & endd.change == T)  endd <- as.Date(startd) + duration

    duration <- abs(as.numeric(as.Date(startd) - as.Date(endd)))








    if(type == "short"){

        import_tick_data(path, startdate = startd, enddate = endd, duration.unit = "days", duration = duration)

      dat <- create_data_week("tickfolder/tickdata_week.csv")
      k <- as.data.frame(dat[[2]]) %>%
        separate(Date_created, c("date", "jour"), sep = " - ")
      endd <- max(unique(as.Date(k$date)), na.rm = T)
      startdate <- min(unique(as.Date(k$date)), na.rm = T)


       cat(" Start date : ", as.character(startd), "\n duration : ", as.character(duration), "\n end date :", as.character(endd), "\n")

       if(report == T) render_report(path = path_report, type.report = "helpdesk", table)


    }


    if(type == "long"){

      import_tick_data(path, enddate = endd, startdate = startd, duration.unit = "months", duration = duration)

      dat <- manipulate_tick_data("tickfolder/tickdata_month.csv")
      k <- as.data.frame(dat[[1]]) %>%
        separate(created.on, c("date", "jour"), sep = "_")

      endd <- max(unique(as.Date(k$date)))
      startdate <- min(unique(as.Date(k$date)))

      cat(" Start date : ", as.character(startd), "\n duration : ", duration, "\n end date :", as.character(endd), "\n")

      if(report == T) render_report(path = path_report, type.report = "helpdesk summary", table)



    }

 }
}


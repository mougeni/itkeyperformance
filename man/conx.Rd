% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/conx.R
\name{conx}
\alias{conx}
\title{Download ping data and provide report}
\usage{
conx(
  startd = NA,
  endd = Sys.Date() - 1,
  duration = 7,
  report = TRUE,
  name_ntwrk = c("4G", "4G-1", "4G-2", "4G-3", "ADSL", "VSAT")
)
}
\arguments{
\item{endd}{The last day of the evaluation of the disconnection}

\item{duration}{number of day to see the evolution ot the disconnection. The end date (endd) should be specified.}

\item{report}{To create the report for the ping}

\item{path}{If you have locally stored the ping data somewhere else, you have to specify the path}

\item{stard}{The first day of the evaluation of the disconnection.}
}
\value{
return a ping report or just message on the files downloaded if you do not specify the option for the report.
}
\description{
Download ping data and provide report
}
\examples{
\dontrun{
conx(stard = "2019-06-19", endd = "2019-06-23", report = TRUE)
 }
}
